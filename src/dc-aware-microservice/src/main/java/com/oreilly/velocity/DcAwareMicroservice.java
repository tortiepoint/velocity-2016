package com.oreilly.velocity;

import com.codahale.metrics.annotation.Counted;
import com.codahale.metrics.annotation.Timed;
import com.google.common.collect.ImmutableMap;
import com.google.inject.Inject;
import com.google.inject.Module;
import com.google.inject.Provides;
import com.google.inject.Scopes;
import com.oreilly.microservices.Microservice;
import com.oreilly.microservices.discovery.ClientFactory;
import com.oreilly.microservices.discovery.ServiceDiscovery;
import com.typesafe.config.Config;
import org.jboss.resteasy.plugins.guice.ext.RequestScopeModule;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import java.util.Map;

public class DcAwareMicroservice extends Microservice {

    public static void main(String... args) {
        new DcAwareMicroservice().run();
    }

    @Override
    public Module[] getModules() {
        return new Module[] {
                new RequestScopeModule() {

                    @Override
                    protected void configure()
                    {
                        bind(DataResource.class).in(Scopes.SINGLETON);
                    }
                }
        };
    }

    @Path("/")
    public static class DataResource {

        @Inject
        private Config config;

        @GET
        @Produces("application/json")
        public Map<String, Boolean> getData() {
            return ImmutableMap.of("value", true);
        }
    }
}
