package com.oreilly.microservices.logging;

import com.typesafe.config.Config;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.MDC;

import javax.inject.Inject;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

public class MdcFilter implements Filter {

    public static final String HEADER_SESSION_ID = "X-Session-ID";
    public static final String MDC_SESSION_ID = "session_id";
    public static final String APPLICATION_NAME = "application_name";

    @Inject
    private Config config;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        String sessionId = ((HttpServletRequest) request).getHeader(HEADER_SESSION_ID);

        if (StringUtils.isNotBlank(sessionId)) {
            MDC.put(MDC_SESSION_ID, sessionId);
        }

        MDC.put(APPLICATION_NAME, config.getString("microservice.name"));

        try {
            chain.doFilter(request, response);
        } finally {
            MDC.remove(MDC_SESSION_ID);
        }
    }

    @Override
    public void destroy() {

    }
}
