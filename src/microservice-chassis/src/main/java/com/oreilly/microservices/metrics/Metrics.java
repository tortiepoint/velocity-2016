package com.oreilly.microservices.metrics;

import com.codahale.metrics.MetricRegistry;

public class Metrics {

    private static MetricRegistry registry = new MetricRegistry();

    public static MetricRegistry defaultRegistry() {
        return registry;
    }
}
