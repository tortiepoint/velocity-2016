package com.oreilly.microservices.metrics;

import com.codahale.metrics.Timer;
import com.codahale.metrics.annotation.Timed;
import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;

public class TimerInterceptor implements MethodInterceptor {

    @Override
    public Object invoke(MethodInvocation invocation) throws Throwable {
        Timed timed = invocation.getStaticPart().getAnnotation(Timed.class);
        String name = timed.name();
        Timer timer = Metrics.defaultRegistry().timer(name);
        Timer.Context context = timer.time();

        try {
            return invocation.proceed();
        } finally {
            context.stop();
        }
    }
}
