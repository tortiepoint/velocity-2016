package com.oreilly.microservices.metrics;

import com.codahale.metrics.ConsoleReporter;
import com.codahale.metrics.JvmAttributeGaugeSet;
import com.codahale.metrics.Reporter;
import com.codahale.metrics.annotation.Counted;
import com.codahale.metrics.annotation.Timed;
import com.codahale.metrics.graphite.Graphite;
import com.codahale.metrics.graphite.GraphiteReporter;
import com.codahale.metrics.jvm.GarbageCollectorMetricSet;
import com.codahale.metrics.jvm.MemoryUsageGaugeSet;
import com.google.inject.Provides;
import com.google.inject.Scopes;
import com.google.inject.Singleton;
import com.google.inject.matcher.Matchers;
import com.typesafe.config.Config;
import org.jboss.resteasy.plugins.guice.ext.RequestScopeModule;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.TimeUnit;

public class MetricsModule extends RequestScopeModule {

    private static Logger LOGGER = LoggerFactory.getLogger(MetricsModule.class);

    @Override
    protected void configure() {
        TimerInterceptor timerInterceptor = new TimerInterceptor();
        CounterInterceptor counterInterceptor = new CounterInterceptor();

        requestInjection(timerInterceptor);
        requestInjection(counterInterceptor);

        bindInterceptor(Matchers.any(), Matchers.annotatedWith(Timed.class),
                timerInterceptor);
        bindInterceptor(Matchers.any(), Matchers.annotatedWith(Counted.class),
                counterInterceptor);

        Metrics.defaultRegistry().registerAll(new JvmAttributeGaugeSet());
        Metrics.defaultRegistry().registerAll(new MemoryUsageGaugeSet());
        Metrics.defaultRegistry().registerAll(new GarbageCollectorMetricSet());

        bind(MetricsFilter.class).in(Scopes.SINGLETON);
    }

    @Provides
    @Singleton
    public Reporter graphiteReporter(Config config) {
        if (config.getBoolean("metrics.enabled")) {
            String host = config.getString("metrics.host");
            int port = config.getInt("metrics.port");

            LOGGER.info("Connecting to Graphite @ {}:{}", host, port);

            GraphiteReporter reporter = GraphiteReporter.forRegistry(Metrics.defaultRegistry())
                    .prefixedWith(config.getString("microservice.name"))
                    .build(new Graphite(host, port));

            reporter.start(1, TimeUnit.SECONDS);

            return reporter;
        } else {
            return ConsoleReporter.forRegistry(Metrics.defaultRegistry()).build();
        }
    }
}
