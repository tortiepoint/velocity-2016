package com.oreilly.microservices.discovery;

import com.google.inject.Inject;
import com.oreilly.microservices.logging.MdcFilter;
import com.oreilly.microservices.metrics.Metrics;
import com.oreilly.microservices.metrics.MetricsFilter;
import com.typesafe.config.Config;
import org.slf4j.MDC;
import retrofit.RestAdapter;
import retrofit.client.OkClient;
import retrofit.converter.JacksonConverter;

public class ClientFactory {

    @Inject
    private ServiceDiscovery serviceDiscovery;

    @Inject
    private Config config;

    @Inject
    public ClientFactory(ServiceDiscovery serviceDiscovery, Config config) {
        this.serviceDiscovery = serviceDiscovery;
        this.config = config;
    }

    public <T> T buildClient(String service, Class<T> klazz) {
        return new RestAdapter.Builder()
                .setEndpoint(new DiscoverableEndpoint(service, serviceDiscovery))
                .setConverter(new JacksonConverter())
                .setClient(new MeasuredClient(service, new OkClient(), Metrics.defaultRegistry()))
                .setRequestInterceptor(request -> {
                    request.addHeader(MetricsFilter.HEADER_HTTP_CLIENT_ID,
                            config.getString("microservice.name"));
                    if(MDC.get(MdcFilter.MDC_SESSION_ID) != null) {
                        request.addHeader(MdcFilter.HEADER_SESSION_ID,
                                MDC.get(MdcFilter.MDC_SESSION_ID));
                    }
                })
                .build().create(klazz);
    }
}
