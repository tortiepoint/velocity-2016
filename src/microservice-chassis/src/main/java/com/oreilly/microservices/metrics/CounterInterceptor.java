package com.oreilly.microservices.metrics;

import com.codahale.metrics.Counter;
import com.codahale.metrics.annotation.Counted;
import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;

public class CounterInterceptor implements MethodInterceptor {

    @Override
    public Object invoke(MethodInvocation invocation) throws Throwable {
        Counted counted = invocation.getStaticPart().getAnnotation(Counted.class);
        String name = counted.name();
        Counter counter = Metrics.defaultRegistry().counter(name);

        try {
            return invocation.proceed();
        } finally {
            counter.inc();
        }
    }

}
