package com.oreilly.microservices.discovery;

import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.Timer;
import retrofit.client.Client;
import retrofit.client.Request;
import retrofit.client.Response;

import java.io.IOException;

public class MeasuredClient implements Client {

    private String service;
    private Client client;
    private MetricRegistry metricRegistry;

    public MeasuredClient(String service, Client client, MetricRegistry metricRegistry) {
        this.service = service;
        this.client = client;
        this.metricRegistry = metricRegistry;
    }

    @Override
    public Response execute(Request request) throws IOException {
        Timer.Context context = metricRegistry.timer(String.format("http-out-%s", service)).time();
        Response response = client.execute(request);
        int statusXx = response.getStatus() / 100;

        metricRegistry.meter(String.format("http-out-%s-%dXX", service, statusXx)).mark();

        context.stop();

        return response;
    }
}
