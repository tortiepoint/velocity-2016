package com.oreilly.microservices.metrics;

import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.Timer;
import org.apache.commons.lang3.StringUtils;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class MetricsFilter implements Filter {

    public static final String HEADER_HTTP_CLIENT_ID = "X-HTTP-Client-ID";

    private MetricRegistry metricRegistry;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        metricRegistry = Metrics.defaultRegistry();
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        String clientId = ((HttpServletRequest) request).getHeader(HEADER_HTTP_CLIENT_ID);
        Timer.Context context = null;

        if (StringUtils.isNotBlank(clientId)) {
            context = metricRegistry.timer(String.format("http-in-%s", clientId)).time();
        }

        chain.doFilter(request, response);

        int statusXx = ((HttpServletResponse) response).getStatus() / 100;

        metricRegistry.meter(String.format("http-in-%dXX", statusXx)).mark();

        if (context != null) {
            context.stop();
        }
    }

    @Override
    public void destroy() {

    }
}
