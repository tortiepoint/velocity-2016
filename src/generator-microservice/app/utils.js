var fetch = require('node-fetch');
var promise = require('promise');
var chalk = require('chalk');
var base = 'http://velocity:velocity@' + process.env.GIT_HOST + ':3000';

function exec(generator, executable, args) {
  return new Promise(function (resolve, reject) {
    var command = generator.spawnCommand(executable, args, {
      cwd: generator.folderPrefix
    });

    command.on('close', function(code) {
      resolve(code);
    });

    command.on('error', function(error) {
      reject(error);
    });
  });
}

module.exports = {
  createRepo: function(name) {
    var url = base + '/api/v1/user/repos'
    var payload = JSON.stringify({ Name: name });
    var auth = 'Basic ' + new Buffer('velocity:velocity').toString('base64');

    return fetch(url, {
      method: 'POST',
      body: payload,
      headers: {
        'Content-Type': 'application/json',
        'Content-Length': Buffer.byteLength(payload),
        'Authorization': auth
      }
    })
    .then(function(res) {
        return res.json();
    });
  },

  initGitRepo: function(generator, name) {
    return exec(generator, 'git', ['init'])
      .then(function() {
        return exec(generator, 'git', ['config', 'user.name', '"velocity"']);
      })
      .then(function() {
        return exec(generator, 'git', ['config', 'user.email', '"not-real@velocity.com"']);
      })
      .then(function() {
        return exec(generator, 'git', ['add', '.']);
      })
      .then(function() {
        return exec(generator, 'git', ['commit', '-m', '"Initial commit by Yeoman."']);
      })
      .then(function() {
        return exec(generator, 'git', ['remote', 'add', 'origin', base + '/velocity/' + name + '.git']);
      })
      .then(function() {
        return exec(generator, 'git', ['push', 'origin', 'master']);
      });
  },

  mavenBuild: function(generator) {
    return exec(generator, 'mvn', ['-o', 'compile']);
  },

  createDashboard: function(generator, name) {
    var url = 'http://admin:admin@' + process.env.GRAFANA_HOST + ':4000/api/dashboards/db';
    var payload = generator.read('/tmp/' + name + '-dashboard.json');

    return fetch(url, {
      method: 'POST',
      body: payload,
      headers: {
        'Content-Type': 'application/json',
        'Content-Length': Buffer.byteLength(payload)
      }
    })
    .then(function(res) {
      return res.json();
    });
  },

  createJenkinsPipeline: function(generator, name) {
    var url = 'http://' + process.env.JENKINS_HOST + ':8888/createItem?name=' + name;
    var payload = generator.read('/tmp/' + name + '.xml');

    return fetch(url, {
      method: 'POST',
      body: payload,
      headers: {
        'Content-Type': 'text/xml',
        'Content-Length': Buffer.byteLength(payload)
      }
    })
    .then(function(res) {
      return res;
    });
  },

  kickOffPipeline: function(name) {
    var url = 'http://' + process.env.JENKINS_HOST + ':8888/job/' + name + '/build?delay=0';

    return fetch(url, {
      method: 'POST'
    })
    .then(function(res) {
      return res;
    });
  },

  ok: function() {
    return chalk.white('[') + chalk.green('OK') + chalk.white(']');
  }
};
