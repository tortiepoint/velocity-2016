'use strict';
var util = require('util');
var path = require('path');
var yeoman = require('yeoman-generator');
var yosay = require('yosay');
var Case = require('case');
var http = require('http');
var chalk = require('chalk');

var utils = require('./utils');

var MicroserviceGenerator = yeoman.Base.extend({
  initializing: function () {
    this.pkg = require('../package.json');
  },

  prompting: function () {
    var done = this.async();

    // Have Yeoman greet the user.
    this.log(yosay(
      'Welcome to the kickass Microservice generator!'
    ));

    var prompts = [{
      type: 'input',
      name: 'serviceName',
      message: 'Enter the name of your service.  (E.G. "my-service")',
      default: this.appname
    }, {
      type: 'input',
      name: 'serviceOwner',
      message: 'Enter the team that owns the service.'
    }, {
      type: 'input',
      name: 'email',
      message: 'Enter an associated email address for this service.'
    }]

    this.prompt(prompts, function (answers) {
      this.serviceNamePascalCase = Case.pascal(answers.serviceName);
      this.serviceNameDashCase = Case.kebab(answers.serviceName);
      this.serviceNameTitleCase = Case.title(answers.serviceName);
      this.serviceOwner = answers.serviceOwner;
      this.email = answers.email;
      this.buildNumberToken = '${BUILD_NUMBER}';
      this.jenkinsHost = process.env.JENKINS_HOST;
      done();
    }.bind(this));
  },

  writing: {
    app: function () {
      var folderPrefix = this.serviceNameDashCase + '/';
      this.folderPrefix = folderPrefix;
      var srcPath = folderPrefix + 'src/main/java/com/oreilly/velocity';

      this.dest.mkdir(srcPath);

      this.template('_.gitignore', folderPrefix + '.gitignore');
      this.template('_pom.xml', folderPrefix + 'pom.xml');
      this.template('_Dockerfile', folderPrefix + 'Dockerfile');
      this.template('_Jenkinsfile', folderPrefix + 'Jenkinsfile');
      this.template('_Microservice.java', srcPath + '/' + this.serviceNamePascalCase + '.java');
      this.template('_pipeline.xml', '/tmp/' + this.serviceNameDashCase + '.xml');
      this.template('_dashboard.json', '/tmp/' + this.serviceNameDashCase + "-dashboard.json");
    }
  },

  end: function () {
    var name = this.serviceNameDashCase;
    var generator = this;

    utils.mavenBuild(generator)
      .then(function() {
        console.log('Creating Git repository "velocity/' + chalk.white(name) +'":');
        return utils.createRepo(name);
      })
      .then(function() {
        return utils.initGitRepo(generator, name);
      })
      .then(function() {
        console.log(utils.ok() + ' Repository created at ' + chalk.blue('http://' + process.env.GIT_HOST + ':3000/velocity/' + name));
        console.log('Creating Grafana dashboard');
      })
      .then(function() {
        return utils.createDashboard(generator, name);
      })
      .then(function() {
        console.log(utils.ok() + ' Dashboard created at ' + chalk.blue('http://' + process.env.GRAFANA_HOST + ':4000'));
        console.log('Creating Jenkins pipeline "' + chalk.white(name) + '"');
      })
      .then(function() {
        return utils.createJenkinsPipeline(generator, name);
      })
      .then(function() {
        return utils.kickOffPipeline(name);
      })
      .then(function() {
        console.log(utils.ok() + ' Pipeline created and started at ' + chalk.blue('http://' + process.env.JENKINS_HOST + ':8888/job/' + name));
      });
  }
});

module.exports = MicroserviceGenerator;
