package com.oreilly.velocity;

import com.google.common.collect.ImmutableMap;
import com.google.inject.Inject;
import redis.clients.jedis.JedisPool;

import javax.ws.rs.*;
import java.util.Map;

@Path("/")
public class DataResource {

    @Inject
    private JedisPool jedisPool;

    @GET
    @Path("{key}")
    @Produces("application/json")
    public Map<String, String> getData(@PathParam("key") String key) {
        return ImmutableMap.of("data", jedisPool.getResource().get(key));
    }

    @POST
    @Produces("application/json")
    @Consumes("application/json")
    public Map<String, Object> putData(Map<String, String> data) {
        jedisPool.getResource().set(data.get("key"), data.get("data"));

        return ImmutableMap.of("data", data.get("data"),
                "ok", true);
    }
}
