package com.oreilly.velocity;

import com.google.inject.Module;
import com.google.inject.Provides;
import com.google.inject.Scopes;
import com.google.inject.Singleton;
import com.oreilly.microservices.Microservice;
import com.typesafe.config.Config;
import org.jboss.resteasy.plugins.guice.ext.RequestScopeModule;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

public class PersistentMicroservice extends Microservice {

    public static void main(String... args) {
        new PersistentMicroservice().run();
    }

    @Override
    public Module[] getModules() {
        return new Module[] {
                new RequestScopeModule() {

                    @Override
                    protected void configure()
                    {
                        bind(DataResource.class).in(Scopes.SINGLETON);
                        bind(RedisHealthCheck.class).in(Scopes.SINGLETON);
                    }

                    @Provides
                    @Singleton
                    public JedisPoolConfig jedisPoolConfig() {
                        JedisPoolConfig jedisPoolConfig = new JedisPoolConfig();

                        jedisPoolConfig.setBlockWhenExhausted(false);
                        jedisPoolConfig.setMaxTotal(-1);

                        return jedisPoolConfig;
                    }

                    @Provides
                    @Singleton
                    public JedisPool jedisPool(Config config, JedisPoolConfig jedisPoolConfig) {
                        return new JedisPool(jedisPoolConfig, config.getString("redis.host"));
                    }
                }
        };
    }

}
