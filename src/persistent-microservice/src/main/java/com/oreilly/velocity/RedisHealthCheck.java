package com.oreilly.velocity;

import com.codahale.metrics.health.HealthCheck;
import com.google.inject.Inject;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

public class RedisHealthCheck extends HealthCheck {

    @Inject
    private JedisPool jedisPool;

    @Override
    protected Result check() throws Exception {
        try {
            if ("PONG".equals(jedisPool.getResource().ping())) {
                return Result.healthy("Redis available");
            } else {
                return Result.unhealthy("Redis ping failed");
            }
        } catch (Exception ex) {
            return Result.unhealthy("Redis unavailable");
        }
    }
}
