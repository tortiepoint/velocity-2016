package com.oreilly.velocity;

import retrofit.http.GET;

import java.util.Map;

public interface OtherMicroserviceClient {

    @GET("/failsRandomly")
    Map<String, String> getRandomlyFailingData();

    @GET("/failsIfToggled")
    Map<String, String> getFailsIfToggled();
}
