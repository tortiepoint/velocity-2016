package com.oreilly.velocity.resource;

import com.codahale.metrics.annotation.Counted;
import com.codahale.metrics.annotation.Timed;
import com.google.common.collect.ImmutableMap;
import com.google.inject.Inject;
import com.oreilly.velocity.OtherMicroserviceClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import java.util.Map;

@Path("/")
public class DataResource {

    private static final Logger logger =
            LoggerFactory.getLogger(DataResource.class);

    @Inject
    private OtherMicroserviceClient otherMicroserviceClient;

    @GET
    @Produces("application/json")
    @Counted(name = "getData")
    @Timed(name = "getDataTime")
    public Map<String, Object> getData() {
        logger.info("Getting some data");
        return ImmutableMap.of("value", true, "version", 2);
    }

    @GET
    @Path("other")
    @Produces("application/json")
    @Timed(name = "getDataFromOther")
    public Map<String, String> getDataFromOther() {
        return otherMicroserviceClient.getRandomlyFailingData();
    }
}
