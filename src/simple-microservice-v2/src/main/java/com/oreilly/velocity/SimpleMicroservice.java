package com.oreilly.velocity;

import com.google.inject.Module;
import com.google.inject.Provides;
import com.google.inject.Scopes;
import com.oreilly.microservices.Microservice;
import com.oreilly.microservices.discovery.ClientFactory;
import com.oreilly.microservices.discovery.ServiceDiscovery;
import com.oreilly.velocity.resource.CircuitBreakerResource;
import com.oreilly.velocity.resource.DataResource;
import com.typesafe.config.Config;
import org.jboss.resteasy.plugins.guice.ext.RequestScopeModule;

public class SimpleMicroservice extends Microservice {

    public static void main(String... args) {
        new SimpleMicroservice().run();
    }

    @Override
    public Module[] getModules() {
        return new Module[] {
                new RequestScopeModule() {

                    @Override
                    protected void configure()
                    {
                        bind(DataResource.class).in(Scopes.SINGLETON);
                        bind(CircuitBreakerResource.class).in(Scopes.SINGLETON);
                    }

                    @Provides
                    public OtherMicroserviceClient otherMicroserviceClient(ServiceDiscovery serviceDiscovery,
                                                                           Config config) {
                        return new ClientFactory(serviceDiscovery, config).buildClient("other-microservice", OtherMicroserviceClient.class);
                    }
                }
        };
    }
}
