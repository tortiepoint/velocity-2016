import groovy.json.JsonSlurper
import groovy.util.XmlSlurper

def gitUrl = 'http://git:3000/velocity/simple-microservice.git'
def image
def version
def appName = 'simple-microservice'

def config = [
    "build": [
        "type": "maven",
        "docker": true
    ],
    "pipeline": [
        [
            "type": "marathon-deploy"
        ]
    ]
]

stage "Build"

node {
    git url: gitUrl

    if (config.build.type == 'maven') {
        pomVersion = new XmlSlurper().parseText(readFile("pom.xml")).version.text()
        version = "${pomVersion}.${env.BUILD_NUMBER}"
        sh 'mvn -o clean install'
    }

    if (config.build.docker) {
        image = "10.140.140.10:5000/${appName}:${version}"
        docker.build(image).push()
    }
}

config.pipeline.each { step ->
    if (step.type == 'marathon-deploy') {
        stage "Deploy to Marathon"
        node {
            git url: 'http://git:3000/velocity/marathon-deploy.git'
        }
    }
}
