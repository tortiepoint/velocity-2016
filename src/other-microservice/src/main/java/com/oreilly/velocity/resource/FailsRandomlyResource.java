package com.oreilly.velocity.resource;

import com.codahale.metrics.annotation.Timed;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.ImmutableMap;
import com.google.inject.Inject;
import org.apache.commons.lang.RandomStringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import java.util.Random;

@Path("/failsRandomly")
public class FailsRandomlyResource {

    private static final Logger logger =
            LoggerFactory.getLogger(FailsRandomlyResource.class);

    private final Random random = new Random();

    @Inject
    private ObjectMapper objectMapper;

    @GET
    @Produces("application/json")
    @Timed(name = "getRandomData")
    public Response getRandomData() throws JsonProcessingException {
        logger.info("There's a random chance this will fail");

        if (random.nextInt(100) > 75) {
            return Response.serverError().entity(objectMapper.writeValueAsString(
                    ImmutableMap.of("error", "An error occurred processing your request")
            )).build();
        }

        return Response.ok(
                objectMapper.writeValueAsString(ImmutableMap.of("value", RandomStringUtils.random(20)))
        ).build();
    }
}
