package com.oreilly.velocity.resource;

import com.google.common.collect.ImmutableMap;
import org.apache.commons.lang.RandomStringUtils;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

@Path("/failsIfToggled")
public class ToggleableFailureResource {

    private AtomicBoolean failing = new AtomicBoolean(false);

    @GET
    @Path("/")
    @Produces("application/json")
    public Map<String, String> getData() {
        if (failing.get()) {
            throw new RuntimeException("failed!");
        } else {
            return ImmutableMap.of("value", RandomStringUtils.random(20));
        }
    }

    @GET
    @Path("/toggle")
    @Produces("application/json")
    public Map<String, Boolean> toggle() {
        failing.set(!failing.get());

        return ImmutableMap.of("failing", failing.get());
    }
}
