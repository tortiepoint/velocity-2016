package com.oreilly.velocity;

import com.google.inject.Module;
import com.google.inject.Scopes;
import com.oreilly.microservices.Microservice;
import com.oreilly.velocity.resource.FailsRandomlyResource;
import com.oreilly.velocity.resource.ToggleableFailureResource;
import org.jboss.resteasy.plugins.guice.ext.RequestScopeModule;

public class OtherMicroservice extends Microservice {

    public static void main(String... args) {
        new OtherMicroservice().run();
    }

    @Override
    public Module[] getModules() {
        return new Module[] {
                new RequestScopeModule() {

                    @Override
                    protected void configure()
                    {
                        bind(FailsRandomlyResource.class).in(Scopes.SINGLETON);
                        bind(ToggleableFailureResource.class).in(Scopes.SINGLETON);
                    }
                }
        };
    }

}
