package com.oreilly.velocity.resource;

import com.google.common.collect.ImmutableMap;
import com.google.inject.Inject;
import com.netflix.hystrix.HystrixCommand;
import com.netflix.hystrix.HystrixCommandGroupKey;
import com.oreilly.velocity.OtherMicroserviceClient;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import java.util.Map;

@Path("/circuitBreaker")
public class CircuitBreakerResource {

    @Inject
    private OtherMicroserviceClient otherMicroserviceClient;

    @GET
    @Path("/data")
    @Produces("application/json")
    public Map<String, String> getData() {
        HystrixCommand.Setter setter = HystrixCommand.Setter
                .withGroupKey(HystrixCommandGroupKey.Factory.asKey("otherMicroserviceData"));
        return new HystrixCommand<Map<String, String>>(setter) {

            @Override
            protected Map<String, String> run() throws Exception {
                return otherMicroserviceClient.getFailsIfToggled();
            }

            @Override
            protected Map<String, String> getFallback() {
                return ImmutableMap.of("value", "Some fallback data");
            }
        }.execute();
    }
}
